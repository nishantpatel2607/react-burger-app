//The file name is set to auxhoc.js as aux.js creates issues in git

const aux = (props) => props.children

export default aux;